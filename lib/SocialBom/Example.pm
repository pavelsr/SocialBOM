package SocialBom::Example;
use strict;
use utf8;
use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use Tie::File;

my $filename1="/home/pavel/socialbom/bom1.txt";
my $filename2="/home/pavel/socialbom/bom2.txt";

# use Mojo::Message::Request;
# use Mojo::Upload;

# This action will render a template
sub welcome {
  my $self = shift;
  $self->render();
}

sub compare {
  my $self = shift;
  my $file1 = $self->param('bom1');
  my $file2 = $self->param('bom2');
  $file1 = $file1->move_to($filename1);
  $file2 = $file2->move_to($filename2);

  # warn Dumper $file1;

  # my $asset = $file->asset;
  # my $headers = $file->headers;
  # my $name = $file->name;
  # my $filename = $file->filename;
  # my $size = $file->size;
  
  # my $bytes1 = $file1->slurp;
  # my $bytes2 = $file2->slurp;

  my $bom1 = convert_to_arr_of_hashes($filename1);	# bom 1 as hashes
  my $bom2 = convert_to_arr_of_hashes($filename2);
  my ($common, $qt_diff1, $unique1, $qt_diff2, $unique2) = filter_two_arrays_of_hashes($bom1, $bom2);
  


  # my $arr2 = convert_to_arr_of_hashes($bytes2);

  # my $upload = Mojo::Upload->new;
  # my $filename = $upload->filename;
  # my $params = $self->req->params;
  # my $file2 = $self->req->upload('bom1');
  # my $filename2 = $file2->filename; 
  # my $file = $self->req->upload('_file');
  # my $file = $self->param('_file');

  # warn Dumper $asset;

  $self->render(
  	bom1 => $bom1,
  	bom2 => $bom2,
  	common => $common,
  	qt_diff1 => $qt_diff1,
  	unique1 => $unique1,
  	qt_diff2 => $qt_diff2,
  	unique2 => $unique2,
  	);
}


sub convert_to_arr_of_hashes {
	my $file = shift;				# content of file
	tie my @file, 'Tie::File', $file or die $!;
	my $hash = {};
	my @arr;
	my $item_strings = 0;

	for my $linenr (0 .. $#file) {
		if ($file[$linenr] =~ /^\w+\s/ ) {
			my @a = split (/   /, $file[$linenr]);
			$hash->{'item'} = $a[0];
			$hash->{'supplier'} = $a[1];
			$hash->{'order_total'} = $a[2];
			
			my @prices;
			for(my $i = 3; $i < scalar @a; $i++) {			# cycle for prices
				# if ( @a[$i] =~ /^([A-Z]{3}\s[0-9]+)\s / ) {
					my @b = split (/ /, $a[$i]);
					my $hash1 = {}; 	
				 	$hash1->{'currency'}=$b[0];
				 	$hash1->{'price'}=$b[1];
				 	$hash1->{'url'}=$b[2];
				 	push (@prices, $hash1);
				 # }
			}
			$hash->{'prices'} = \@prices;   



			my $j=1;
			my @usage;
			while ($file[$linenr+$j] =~ /^\s\s\s/) {
				my @c = split (/   /, $file[$linenr+$j]);
				my $hash2 = {};
				if ($c[1] =~ /^\d+\/\d+/) {
					my @totalbers = split (/\//, $c[1]);
					$hash->{'total'} = $totalbers[1];
					$hash2->{'qt'} = $totalbers[0];
					} 
				else {
						$hash->{'total'} = $c[1];
					}
			    $hash2->{'subsystem'} = $c[2];
			    $hash2->{'use'} = $c[3];
			    push @usage, $hash2;
				$j++;
			}
			$hash->{'usage'} = \@usage;

			push @arr, $hash;
			$hash={};
			$item_strings++;
		}
	}
	untie @file;
	 my @sorted = sort { $a->{item} <=> $b->{item} } @arr;
 	return \@sorted;
}



sub filter_two_arrays_of_hashes {
  my ($arr1, $arr2) = @_;
  my (@equals, @qt_diff1, @unique1, @qt_diff2, @unique2);
  for (@$arr1) {
    my $curr= $_;
    # warn $curr->{item};
    my $item_match = grep { $_->{item} eq $curr->{item} } @$arr2;
    my $qt_match = grep { $_->{total} eq $curr->{total} } @$arr2;
    my $criteria = $item_match.$qt_match;
    # warn $criteria;
    if($criteria eq "11") {
      print "Full match\n";
      push @equals, $curr;
    } elsif($criteria eq "10") {
      print "Qt difference\n";
      push @qt_diff1, $curr;
    } else {
      print "Unique matched\n";
      push @unique1, $curr;
    }
  } # end first for
  for (@$arr2) {
    my $curr2= $_;
    my $item_match = grep { $_->{item} eq $curr2->{item} } @$arr1;
    my $qt_match = grep { $_->{total} eq $curr2->{total} } @$arr1;
    my $criteria = $item_match.$qt_match;
    if($criteria eq "11") {
      print "Full match\n";
    } elsif($criteria eq "10") {
      print "Qt difference\n";
      push @qt_diff2, $curr2;
    } else {
      print "Unique matched\n";
      push @unique2, $curr2;
    }
  }
return (\@equals, \@qt_diff1, \@unique1, \@qt_diff2, \@unique2);
}




# sub compare_arr_of_hashes {
# 	my ($arr1, $arr2) = @_;
# 	my (@common, @qt_diff1, @unique1, @qt_diff2, @unique2);
# 	for (@$arr1) {
# 		my $k = $_;
# 		for (@$arr2) {
# 			if ($_->{item} eq $k->{item}) {
# 				if ($_->{total} eq $k->{total}) {
# 					push @common, $k;
# 					} else {
# 					push @qt_diff1, $k;
# 					}
# 			} else {
# 				push @unique1, $k;
# 			}
# 		}
# 	}

# }	
# 	for (@$arr2) {
# 		my $k1 = $_;
# 		for (@$arr1) {
# 			if ($_->{item} eq $k1->{item}) {
# 				if ($_->{total} eq $k1->{total}) {
# 					next;
# 					} else {
# 					push @qt_diff2, $k1;
# 					}
# 			} else {
# 				push @unique2, $k1;
# 			}
# 		}
# 	}
# 	warn Dumper @unique1;
# 	return (\@common, \@qt_diff1, \@unique1, \@qt_diff2, \@unique2);
# 	# compare procedure for hashes with different sizes but equal keys
# }




1;
